__version__ = '0.0.0'

import uuid
import json
from workflow_publisher.rabbitmq_client import RabbitMQClient

__all__ = ['WorkflowQueue']


class WorkflowQueue:
    def __init__(self, host, port, username, password, exchange):
        self.rmq_client = RabbitMQClient(host, port, username, password)
        self.rmq_exchange = exchange

    def put(self, tasks, context_id, context_type, retry_count=10):
        guid = str(uuid.uuid4())
        message = {'guid': guid, 'context_id': context_id, 'context_type': context_type, 'tasks': tasks,
                   'retry_count': retry_count}
        data = {"name": "LP_WORKFLOW", "body": message}
        self.rmq_client.publish(json.dumps(data), self.rmq_exchange, "LP_WORKFLOW")
        return guid
