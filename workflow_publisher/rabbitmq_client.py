import logging
import time
import pika


class RabbitMQClient(object):
    def __init__(self, host, port, username, password, queue_name=None):
        self.host = host
        self.port = port
        self.queue_name = queue_name
        self.username = username
        self.password = password
        self.credentials = pika.PlainCredentials(username, password)

    def connect(self):
        self.connection = pika.SelectConnection(
            pika.ConnectionParameters(host=self.host, port=self.port, credentials=self.credentials),
            self.on_connection_open, stop_ioloop_on_close=False)
        self.connection.ioloop.start()

    def on_connection_open(self, unused_connection):
        logging.debug('Connection opened')
        self.connection.add_on_close_callback(self.on_connection_closed)

        self.connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        logging.debug('Channel opened')
        self.channel = channel
        self.channel.add_on_close_callback(self.on_channel_closed)
        self.channel.queue_declare(self.on_queue_declareok, queue=self.queue_name, durable=True)

    def on_queue_declareok(self, method_frame):
        # channel.basic_qos(prefetch_count=1) # if you want to restrict consumer to listen one message at a time
        self.channel.basic_consume(self.callback, queue=self.queue_name)
        # self.channel.start_consuming()

    def get_channel_for_publish_event(self):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port, credentials=self.credentials))
        self.channel = self.connection.channel()

    def publish(self, message, exchange='', routing_key=''):
        self.get_channel_for_publish_event()
        logging.debug("Publishing message = %s on queue = %s" % (message, routing_key))
        self.channel.basic_publish(exchange=exchange,
                                   routing_key=routing_key,
                                   body=message,
                                   properties=pika.BasicProperties(
                                       delivery_mode=2,  # make message persistent
                                       content_type='application/json'
                                   ))
        self.connection.close()

    def on_connection_closed(self, connection, reply_code, reply_text):
        logging.info('Connection closed, reopening in 5 seconds: (%s) %s', (reply_code, reply_text))
        self.connection.add_timeout(5, self.reconnect)

    def on_channel_closed(self, channel, reply_code, reply_text):
        logging.info('Channel %i was closed: (%s) %s', channel, reply_code, reply_text)
        self.connection.close()

    def reconnect(self):
        if self.connection:
            self.connection.ioloop.stop()
        self.connection = None
        self.channel = None
        try:
            self.connect()
        except Exception as e:
            logging.error("Could Not connect to rabbitMQ client. Re-trying after 5 seconds")
            time.sleep(5)
            self.reconnect()

    def close(self):
        self.connection.close()
