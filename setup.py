from setuptools import setup

package_name = "workflow_publisher"

packages = [package_name]

setup(
    name=package_name,
    version='0.0.2',
    description='client library for workflow queue',
    url='git@bitbucket.org:livspaceeng/workflow_publisher.git',
    author='Vijay Jain',
    author_email='vijay.jain@livspace.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],

    keywords=['workflow', 'tasks'],
    packages=packages,
    install_requires=[
        'pika==0.10.0'
    ],
    extras_require={},
    package_data={},
    data_files=[],
    entry_points={},
)
